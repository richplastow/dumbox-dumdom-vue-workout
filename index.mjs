//// Xx.
const appState = { subs:[], cliFocus:true, layoutScrollY:0, top:`
The Escape key toggles focus between CLI + GUI.
~ Type <b>help</b> for help<br>
~ Type <A href="#">exit</a> to quit.<br>
<small>Small <b>Bold<i>Inv</i></b></small>`
}
appState.toggleCliFocus = function () {
    appState.cliFocus = ! appState.cliFocus
    dumbox.render()
}
appState.scrollLayout = function (relY) {
    appState.layoutScrollY =
        Math.max(0, appState.layoutScrollY + relY) //@TODO clamp at other end
    dumbox.render()
}
appState.scrollLayoutTo = function (absY) {
    //@TODO
    dumbox.render()
}

//// Create a Dumbox instance.
import Dumbox from './lib/dumbox.mjs'

//// If we’re in Node.js, polyfill `window` and `document` to global scope.
import './lib/dumdom.mjs'

//// Create a Dumcli instance, and pass it our Vue component’s state @TODO.
//// If we’re in a browser, this will polyfill Node.js’s `process` object.
import Dumcli from './lib/dumcli.mjs'
const dumcli = new Dumcli(appState)

//// This is just standard version of Vue, just given an `export default`.
import Vue from './lib/vue-converted-to-module.mjs'
Vue.config.productionTip = false // makes clearing the console easier

//// Define <my-component>.
Vue.component('my-component', {
    template: '<pre><div v-for="sub in subs">{{ sub }}</div><span v-html="top"></span><hr></pre>'
    // template: '<pre><b>Output:</b><br><span v-html="top"></span><hr></pre>'
    // template: '<pre><b>Output:</b><br>{{ top }}<hr></pre>'
  , data: function () { return appState }
  , updated: function () {
        this.$nextTick( () => {
            dumbox.render()
            dumcli.render()
        })
    }
})

//// Create containers for the Vue component.
const $article = document.createElement('article')
const $plain1 = document.createTextNode('Plain 1 ')
const $bold = document.createElement('b')
$bold.textContent = 'Bold Text '
const $boldInverse = document.createElement('i')
$boldInverse.textContent = 'Bold Inverse Text'
const $bold2 = document.createTextNode(' Bold 2 ')
const $plain2 = document.createTextNode('Plain 3 ')
const $em = document.createElement('em')
$em.textContent = ' Emphasised Text'
const $strike = document.createElement('s')
$strike.textContent = ' Strike Text '
const $underline = document.createElement('u')
$underline.textContent = ' Underline Text'
const $br1 = document.createElement('br')
// const $br2 = document.createElement('br')
// const $br3 = document.createElement('br')
const $myComponent = document.createElement('my-component')
document.body.appendChild($article)
$article.appendChild($plain1)
$article.appendChild($bold)
$bold.appendChild($boldInverse)
$bold.appendChild($bold2)
$article.appendChild($plain2)
$article.appendChild($em)
$article.appendChild($strike)
$article.appendChild($br1)
// $article.appendChild($br2)
// $article.appendChild($br3)
$article.appendChild($underline)
$article.appendChild($myComponent)

//// Boot Vue.
new Vue({
    el:'article'
})

//// Begin rendering the Dumdom’s `window`.
const dumbox = new Dumbox(appState, window, process.stdout, 31, 15)

//// Xx.
if ('object' === typeof window && window.addEventListener)
    window.addEventListener('load', evt => {
        // document.querySelector('.dumbox-ignore h1').innerHTML += ' ' + appMeta.version
        dumcli.setUpBrowserInput( document.querySelector('#dumcli-input') )
        dumcli.setUpBrowserOutput( document.querySelector('#dumcli pre'), 31, 16 )
        dumbox.render()
        dumcli.render()
    })
else
    dumbox.render(), dumcli.render()
