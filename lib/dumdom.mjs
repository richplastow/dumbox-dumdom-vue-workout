const revealCalls = false

//// Dumdom
//// ======
////
//// #### A minimal Document Object Model for running Vue.js in Node.js
////
//// Dumdom can be used with Dumbox to make minimal Vue.js apps in the terminal.
////
//// Dumdom borrows heavily from two MIT licensed NPM packages:
//// ‘HTML Element’ by Ayman Mackouly npmjs.com/package/html-element
//// ‘Node Html Parser’ by Chris Winberry npmjs.com/package/node-html-parser
////
////
//// dumdom.mjs
//// ----------
////
//// - Parse
//// - Utility
//// - Attribute
//// - Style
//// - DOMTokenList
//// - MutationObserver
//// - Node
//// - CharacterData
//// - Comment
//// - TextNode
//// - Element
//// - Document
//// - Dumdom




//// PARSE

//// Define RegExps used by parseIntoChildNodes() and parseAndSetAttributes().
const markupRx // parses the tags from a raw HTML string
  = /<!--[^]*?(?=-->)-->|<(\/?)([a-z][-.0-9_a-z]*)\s*([^>]*?)(\/?)>/ig
const attributeRx // parses attributes from the innards of a tag
  = /(^|\s)(id|class)\s*=\s*("([^"]+)"|'([^']+)'|(\S+))/ig


//// Define lookup-tables used by parseIntoChildNodes().
const selfClosingTags = { meta:1,img:1,link:1,input:1,area:1,br:1,hr:1 }


//// Replaces an element’s `childNodes` with the parsed version of a string.
function parseIntoChildNodes ($el, data, options) {
    let match
      , currentParent = $el
      , stack = [ $el ]
      , lastTextPos = -1 // the start-position of the most recent text (defaults to show no text has been found)

    //// To capture TextNodes on the top level, wrap the entire html string in a
    //// temporary tag. parseIntoChildNodes() removes this before it returns.
    data = `<dumdom-tmp>${data}</dumdom-tmp>`//@TODO rewrite markupRx to avoid this hackery

    //// Begin with an empty `childNodes` array.
    $el.childNodes.splice(0, $el.childNodes.length)

    //// Step through the raw HTML string’s markup.
    while ( match = markupRx.exec(data) ) {
        const lastIndex = markupRx.lastIndex

        //// Add a TextNode if we found text content.
        if (-1 !== lastTextPos && lastTextPos + match[0].length < lastIndex) {
            const text = data.substring(lastTextPos, lastIndex - match[0].length)
            const $textNode = document.createTextNode(text)
            currentParent.appendChild($textNode)
        }

        lastTextPos = lastIndex

        //// Ignore comments.
        if ('!' === match[0][1]) continue

        //// Convert the tag name to lowercase, so <EM> becomes <em>.
        match[2] = match[2].toLowerCase()

        //// Deal with any kind of tag apart from a closing tag, so not </em>.
        if (! match[1]) { // would be '/' if </em>

            //@TODO kElementsClosedByOpening

            //// Create the sub-element and append it to the current parent.
            const $subel = document.createElement(match[2])
            parseAndSetAttributes($subel, match[3])
            currentParent.appendChild($subel)

            //// Move down a level in the hierarchy.
            currentParent = $subel
            stack.push(currentParent)

            ////@TODO if (kBlockTextElements[match[2]]

        }

        //// Deal with a closing tag, </em>, explicit self-closing tag, <div/>,
        //// or an implicit self-closing tag, <br>.
        if ( match[1] || match[4] || selfClosingTags[match[2]] ) {

            //// If the tags don’t match, flag this as invalid markup...
            if (match[2] !== currentParent.tagName) break //@TODO handle unmatching tags

            //@TODO kElementsClosedByClosing

            //// ...otherwise, move up a level in the hierarchy.
            stack.pop()
            currentParent = stack[stack.length-1]
        }

    }

    //// Remove the temporary outermost tag. @TODO rewrite markupRx to avoid this hackery
    if (1 !== $el.childNodes.length || 'dumdom-tmp' !== $el.childNodes[0].nodeName)
        throw Error('Expected one <dumdom-tmp>')
    const realChildNodes = $el.childNodes[0].childNodes
    $el.childNodes.splice(0, 1) // clear all child nodes
    $el.childNodes = $el.childNodes.concat(realChildNodes)

}

//// Parses attributes from a string and sets them on a given Element.
//// Used by parseIntoChildNodes().
function parseAndSetAttributes ($el, rawAttrs) {
    for (let match; match=attributeRx.exec(rawAttrs);) {
        $el.setAttribute( match[2], match[4] || match[5] || match[6] )
    }
}




//// UTILITY

//@TODO remove escapeHTML() if not needed
// function escapeHTML(s) {
//     return String(s)
//        .replace(/&/g, '&amp;')
//        .replace(/</g, '&lt;')
//        .replace(/>/g, '&gt;')
// }

function escapeAttribute(s) {
    return escapeHTML(s).replace(/"/g, '&quot;')
}

//// Property to attribute names.
const PROPS_TO_ATTRS = {
    'className': 'class'
  , 'htmlFor': 'for'
}

function propToAttr (prop) {
  return PROPS_TO_ATTRS[prop] || prop
}

//// Map of attributes to the elements they affect.
//// developer.mozilla.org/en-US/docs/Web/HTML/Attributes
const HTML_ATTRIBUTES = {
    'accept': new Set([ 'form', 'input' ])
  , 'accept-charset': new Set([ 'form' ])
  , 'accesskey': 'GLOBAL'
  , 'action': new Set([ 'form' ])
  , 'align': new Set([ 'applet', 'caption', 'col', 'colgroup', 'hr', 'iframe'
      , 'img', 'table', 'tbody', 'td', 'tfoot', 'th', 'thead', 'tr' ])
  , 'alt': new Set([ 'applet', 'area', 'img', 'input'])
  , 'async': new Set([ 'script' ])
  , 'autocomplete': new Set([ 'form', 'input' ])
  , 'autofocus': new Set([ 'button', 'input', 'keygen', 'select', 'textarea' ])
  , 'autoplay': new Set([ 'audio', 'video' ])
  , 'autosave': new Set([ 'input' ])
  , 'bgcolor': new Set([ 'body', 'col', 'colgroup', 'marquee', 'table', 'tbody'
      , 'tfoot', 'td', 'th', 'tr' ])
  , 'border': new Set([ 'img', 'object', 'table' ])
  , 'buffered': new Set([ 'audio', 'video' ])
  , 'challenge': new Set([ 'keygen' ])
  , 'charset': new Set([ 'meta', 'script' ])
  , 'checked': new Set([ 'command', 'input' ])
  , 'cite': new Set([ 'blockquote', 'del', 'ins', 'q' ])
  , 'class': 'GLOBAL'
  , 'code': new Set([ 'applet' ])
  , 'codebase': new Set([ 'applet' ])
  , 'color': new Set([ 'basefont', 'font', 'hr' ])
  , 'cols': new Set([ 'textarea' ])
  , 'colspan': new Set([ 'td', 'th', ])
  , 'content': new Set([ 'meta' ])
  , 'contenteditable': 'GLOBAL'
  , 'contextmenu': 'GLOBAL'
  , 'controls': new Set([ 'audio', 'video' ])
  , 'coords': new Set([ 'area' ])
  , 'data': new Set([ 'object' ])
  , 'datetime': new Set([ 'del', 'ins', 'time' ])
  , 'default': new Set([ 'track' ])
  , 'defer': new Set([ 'script' ])
  , 'dir': 'GLOBAL'
  , 'dirname': new Set([ 'input', 'textarea' ])
  , 'disabled': new Set([ 'button', 'command', 'fieldset', 'input', 'keygen'
      , 'optgroup', 'option', 'select', 'textarea' ])
  , 'download': new Set([ 'a', 'area' ])
  , 'draggable': 'GLOBAL'
  , 'dropzone': 'GLOBAL'
  , 'enctype': new Set([ 'form' ])
  , 'for': new Set([ 'label', 'output' ])
  , 'form': new Set([ 'button', 'fieldset', 'input', 'keygen', 'label', 'meter'
      , 'object', 'output', 'progress', 'select', 'textarea' ])
  , 'formaction': new Set([ 'input', 'button' ])
  , 'headers': new Set([ 'td', 'th' ])
  , 'height': new Set([ 'canvas', 'embed', 'iframe', 'img', 'input', 'object'
      , 'video' ])
  , 'hidden': 'GLOBAL'
  , 'high': new Set([ 'meter' ])
  , 'href': new Set([ 'a', 'area', 'base', 'link' ])
  , 'hreflang': new Set([ 'a', 'area', 'link' ])
  , 'http-equiv': new Set([ 'meta' ])
  , 'icon': new Set([ 'command' ])
  , 'id': 'GLOBAL'
  , 'ismap': new Set([ 'img' ])
  , 'itemprop': 'GLOBAL'
  , 'keytype': new Set([ 'keygen' ])
  , 'kind': new Set([ 'track' ])
  , 'label': new Set([ 'track' ])
  , 'lang': 'GLOBAL'
  , 'language': new Set([ 'script' ])
  , 'list': new Set([ 'input' ])
  , 'loop': new Set([ 'audio', 'bgsound', 'marquee', 'video' ])
  , 'low': new Set([ 'meter' ])
  , 'manifest': new Set([ 'html' ])
  , 'max': new Set([ 'input', 'meter', 'progress' ])
  , 'maxlength': new Set([ 'input', 'textarea' ])
  , 'media': new Set([ 'a', 'area', 'link', 'source', 'style' ])
  , 'method': new Set([ 'form' ])
  , 'min': new Set([ 'input', 'meter' ])
  , 'multiple': new Set([ 'input', 'select' ])
  , 'muted': new Set([ 'video' ])
  , 'name': new Set([ 'button', 'form', 'fieldset', 'iframe', 'input', 'keygen'
      , 'object', 'output', 'select', 'textarea', 'map', 'meta', 'param' ])
  , 'novalidate': new Set([ 'form' ])
  , 'open': new Set([ 'details' ])
  , 'optimum': new Set([ 'meter' ])
  , 'pattern': new Set([ 'input' ])
  , 'ping': new Set([ 'a', 'area' ])
  , 'placeholder': new Set([ 'input', 'textarea' ])
  , 'poster': new Set([ 'video' ])
  , 'preload': new Set([ 'audio', 'video' ])
  , 'radiogroup': new Set([ 'command' ])
  , 'readonly': new Set([ 'input', 'textarea' ])
  , 'rel': new Set([ 'a', 'area', 'link' ])
  , 'required': new Set([ 'input', 'select', 'textarea' ])
  , 'reversed': new Set([ 'ol' ])
  , 'rows': new Set([ 'textarea' ])
  , 'rowspan': new Set([ 'td', 'th' ])
  , 'sandbox': new Set([ 'iframe' ])
  , 'scope': new Set([ 'th' ])
  , 'scoped': new Set([ 'style' ])
  , 'seamless': new Set([ 'iframe' ])
  , 'selected': new Set([ 'option' ])
  , 'shape': new Set([ 'a', 'area' ])
  , 'size': new Set([ 'input', 'select' ])
  , 'sizes': new Set([ 'img', 'link', 'source' ])
  , 'span': new Set([ 'col', 'colgroup' ])
  , 'spellcheck': 'GLOBAL'
  , 'src': new Set([ 'audio', 'embed', 'iframe', 'img', 'input', 'script'
      , 'source', 'track', 'video' ])
  , 'srcdoc': new Set([ 'iframe' ])
  , 'srclang': new Set([ 'track' ])
  , 'srcset': new Set([ 'img' ])
  , 'start': new Set([ 'ol' ])
  , 'step': new Set([ 'input' ])
  , 'style': 'GLOBAL'
  , 'summary': new Set([ 'table' ])
  , 'tabindex': 'GLOBAL'
  , 'target': new Set([ 'a', 'area', 'base', 'form' ])
  , 'title': 'GLOBAL'
  , 'type': new Set([ 'button', 'input', 'command', 'embed', 'object', 'script'
      , 'source', 'style', 'menu' ])
  , 'usemap': new Set([ 'img', 'input', 'object' ])
  , 'value': new Set([ 'button', 'option', 'input', 'li', 'meter', 'progress'
      , 'param' ])
  , 'width': new Set([ 'canvas', 'embed', 'iframe', 'img', 'input', 'object'
      , 'video' ])
  , 'wrap': new Set([ 'textarea' ])
}

function isStandardAttribute (attrName, tagName) {
  tagName = tagName.toLowerCase()
  const attr = HTML_ATTRIBUTES[ attrName.toLowerCase() ]
  return !! attr && ( attr === 'GLOBAL' || attr.has(tagName) )
}
//// ATTRIBUTE

//// This does not relate to an actual DOM class, though it’s related to
//// developer.mozilla.org/en-US/docs/Web/API/Attr
//// Dumdom just uses it in Element’s setAttribute() method.
class Attribute {
    constructor (name, value) {
        if (name) {
            this.name = name
            this.value = value ? value : ''
        }
    }
}




//// STYLE

//// developer.mozilla.org/en-US/docs/Web/API/CSSStyleDeclaration
//// Used by Element’s constructor() to make its `style` property.
//// @TODO item(), getPropertyPriority(), removeProperty(), parentRule()
class CSSStyleDeclaration {

    constructor (el) {
        this.el = el
        this.styles = []
    }

    get length () {
        if (revealCalls) console.log('get Style length', this.styles.length)
        return this.styles.length
    }

    get cssText () {
        let out = ''
        this.styles.forEach( s => out += s.name + ':' + s.value + ';' )
        if (revealCalls) console.log('get cssText', out)
        return out
    }

    set cssText (v) {
        if (revealCalls) console.log('set cssText', v)
        this.styles = []
        v.split(';').forEach( part => {
            const splitPoint = part.indexOf(':')
            if (! splitPoint) return
            const key   = part.slice(0, splitPoint).trim()
            const value = part.slice(splitPoint+1).trim()
            this.setProperty(key, value)
        }, this)
    }

    getProperty (n) { //@TODO should be getPropertyValue(), I think
        if (revealCalls) console.log('Style getProperty', n)
        return this.el._getProperty(this.styles, n)
    }

    setProperty (n, v) { //@TODO `priority` argument
        if (revealCalls) console.log('Style setProperty', n, v)
        this.el._setProperty(this.styles, { name:n, value:v })
    }

}




//// DOMTOKENLIST

//// developer.mozilla.org/en-US/docs/Web/API/DOMTokenList
//// Used by Element’s constructor() to make its `classList` property.
class DOMTokenList {
    constructor () {
        this._tokens = []
    }

    get length () {
        return this._tokens.length
    }

    get value () {
        return this._tokens.join(' ')
    }
    set value (v) {
        if ('' === v) return this._tokens = []
        this._tokens = v.split(' ') //@TODO parse, eg collapse double spaces
    }

    add (...tokens) {
        tokens.map( token => {
            if ( this._tokens.includes(token) ) return
            this._tokens.push(token)
        })
    }

    contains (token) {
        return this._tokens.includes(token)
    }

    item (index) {
        return this._tokens[index]
    }

    remove (...tokens) {
        this._tokens = this._tokens.filter( token => ! tokens.includes(token) )
    }

    replace (oldToken, newToken) {
        const index = this._tokens.indexOf(oldToken)
        if (-1 < index) this._tokens[index] = newToken
    }

    toggle (token, force) { //@TODO implement `force`
        if ( this.contains(token) ) {
            this.remove(token)
            return false
        }
        this.add(token)
        return true
    }
}




//// MUTATIONOBSERVER

//// developer.mozilla.org/en-US/docs/Web/API/MutationObserver
//// Watches for changes made to the DOM tree.
////@TODO takeRecords()
////@TODO remove this if not needed
class MutationObserver {

    //// developer.mozilla.org/en-US/docs/Web/API/MutationObserver/MutationObserver
    //// Creates a new observer which invokes `callback` when DOM events occur.
    constructor (callback) {
        this._options = {}
        this._mutationList = []
    }

    //// developer.mozilla.org/en-US/docs/Web/API/MutationObserver/disconnect
    //// Tells the observer to stop watching for mutations. The observer can be
    //// reused by calling its observe() method again.
    disconnect () {
        //@TODO implement
    }

    //// developer.mozilla.org/en-US/docs/Web/API/MutationObserver/observe
    //// Begin receiving notifications of changes to the DOM that match the
    //// given options.
    observe (target, options={}) { // or a MutationObserverInit instance
        this._options = options
        //@TODO implement
    }
}


//// NODE

//// developer.mozilla.org/en-US/docs/Web/API/Node
//// The base class for Document, Element and CharacterData.
//// Nothing to do with Node.js, of course!
//// @TODO nextSibling, ownerDocument, previousSibling
//// @TODO compareDocumentPosition(), contains(), hasChildNodes(), insertBefore()
//// @TODO isEqualNode(), normalize(), replaceChild()
class Node {

    constructor () {
        this.parentElement = null //@TODO `parentElement` should be read-only - maybe have a `_parentElement` shadow
        this.childNodes = [] //@TODO `childNodes` should be read-only - maybe have a `_childNodes` shadow
    }

    //// developer.mozilla.org/en-US/docs/Web/API/Node/firstChild
    get firstChild () {
        if (revealCalls) console.log(this.nodeName, 'get firstChild', this.childNodes[0] ? this.childNodes[0].nodeName : null)
        return this.childNodes[0] || null
    }

    //// developer.mozilla.org/en-US/docs/Web/API/Node/innerText
    //// innerText is the VISIBLE text, as if a user selected it.
    //// innerText also converts <BR> to newline (which textContent does not).
    ////@TODO proper implementation of innerText
    // get innerText () {
    //     let s = ''
    //     // if (this.childNodes.html) //@TODO avoid this hack - parse the value when innerHTML is set
    //     //     s = this.childNodes.html
    //     // else
    //     this.childNodes.forEach( e => s += e.textContent )
    //     if (revealCalls) console.log(this.nodeName, 'get innerText', s)
    //     return s
    // }
    // set innerText (v) { //@TODO implement setting innerText
    //     if (revealCalls) console.log(this.nodeName, 'set innerText', `'${v}'`)
    // }

    //// developer.mozilla.org/en-US/docs/Web/API/Node/textContent
    //// textContent is ALL the text, including <script> and <style> elements.
    get textContent () {
        let s = ''
        // if (this.childNodes.html) //@TODO avoid this hack - parse the value when innerHTML is set
        //     s = this.childNodes.html
        // else
        this.childNodes.forEach( e => s += e._textContent )
        if (revealCalls) console.log(this.nodeName, 'Node get textContent', this.childNodes.length, `'${s}'`)
        return s
    }
    set textContent (v) {
        if (revealCalls) console.log(this.nodeName, 'Node set textContent', `'${v}'`)
        const t = new TextNode()
        t.textContent = v
        this.appendChild(t)
        return v
    }

    //// developer.mozilla.org/en-US/docs/Web/API/Node/lastChild
    get lastChild () {
        const l = this.childNodes.length
        if (revealCalls) console.log(this.nodeName, 'get lastChild', l ? this.childNodes[l-1].nodeName : null)
        return this.childNodes[l-1] || null
    }

    //// developer.mozilla.org/en-US/docs/Web/API/Node/nodeValue
    //// Dumdom only implements `nodeValue` for Comment and TextNode
    get nodeValue () {
        if (revealCalls) console.log(this.nodeName, 'get nodeValue')
        return null
    }
    set nodeValue (v) { //@TODO implement
        if (revealCalls) console.log(this.nodeName, 'set nodeValue', `'${v}'`)
    }

    //// developer.mozilla.org/en-US/docs/Web/API/Node/parentNode
    get parentNode () {
        return this.parentElement //@TODO actually might be different to parentElement, if...
    } //@TODO ...parentNode is a Node but not an Element (in which case parentElement should be null)

    //// developer.mozilla.org/en-US/docs/Web/API/Node/appendChild
    appendChild (aChild) {
        if (revealCalls) console.log(this.nodeName, 'appendChild', aChild.nodeName)
        aChild.parentElement = this
        this.childNodes.push(aChild)
        return aChild
    }

    //// developer.mozilla.org/en-US/docs/Web/API/Node/cloneNode
    cloneNode (deep) {
        if (revealCalls) console.log(this.nodeName, 'cloneNode', deep)
        const el = new Element() //@TODO deal with edge-case, cloning document
        el.tagName = this.tagName
        return el
    }

    //// developer.mozilla.org/en-US/docs/Web/API/Node/removeChild
    removeChild (rChild) {
        if (revealCalls) console.log(this.nodeName, 'removeChild', rChild.nodeName)
        let removed = false
        this.childNodes.forEach( (child, index) => {
            if (child === rChild) {
              this.childNodes.splice(index, 1) // splice keeps `childNode` clean
              rChild.parentElement = null
              removed = true
            }
        })
        if (removed) return rChild
    }
}




//// CHARACTERDATA

//// developer.mozilla.org/en-US/docs/Web/API/CharacterData
//// The base class for Comment and TextNode.
////@TODO nextElementSibling, previousElementSibling
////@TODO appendData(), deleteData(), insertData(), replaceData(), substringData()
class CharacterData extends Node {
    constructor () {
        super()
    }

    //@TODO remove these two commented lines... I think
    // get data () { return this.textContent }
    // set data (v) { this.textContent = v }

    get length () { return this.textContent.length }
}




//// COMMENT

//// developer.mozilla.org/en-US/docs/Web/API/Comment
//// Used by Document’s createComment() method.
class Comment extends CharacterData {
    constructor () {
        super()
        this.data = '' //@TODO or null?
    }

    get nodeType () { return 8 }
    get nodeName () { return '#comment' }

    //// developer.mozilla.org/en-US/docs/Web/API/Node/nodeValue
    get nodeValue () {
        const v = escapeHTML(this.data || '')
        if (revealCalls) console.log('Comment get nodeValue', `'${v}'`)
        return v
    }
    set nodeValue (v) { //@TODO implement
        if (revealCalls) console.log('Comment set nodeValue', `'${v}'`)
        return this.data = v+''
    }
}




//// TEXTNODE

//// developer.mozilla.org/en-US/docs/Web/API/Text
//// Used by Document’s createTextNode() method.
////@TODO wholeText, assignedSlot, splitText()
class TextNode extends CharacterData {

    get nodeType () { return 3 }
    get nodeName () { return '#text' }

    //// developer.mozilla.org/en-US/docs/Web/API/Node/textContent
    //// Note that TextNode overrides the Node class’s getter and setter.
    get textContent () {
        // const v = escapeHTML(this._textContent || '')
        const v = this._textContent || ''
        if (revealCalls) console.log('TextNode get textContent', `'${v}'`)
        return v
    }
    set textContent (v) {
        this._textContent = v+''
        // console.log('.'.repeat(40) + v);
        // if (v !== this._textContent) console.log('.'.repeat(40), 'TextNode set textContent\n', '.'.repeat(40), `IN: '${v}'\n`, '.'.repeat(40), `OUT: '${this._textContent}'`)
        if (revealCalls) console.log('TextNode set textContent', `'${v}'`)
        return this._textContent = v+''
    }

    //// developer.mozilla.org/en-US/docs/Web/API/Node/nodeValue
    get nodeValue () {
        // const v = escapeHTML(this._textContent || '')
        const v = this._textContent || ''
        if (revealCalls) console.log('TextNode get nodeValue', `'${v}'`)
        return v
    }
    set nodeValue (v) { //@TODO implement
        if (revealCalls) console.log('TextNode set nodeValue', `'${v}'`)
        return this._textContent = v+''
    }
}




//// ELEMENT

//// developer.mozilla.org/en-US/docs/Web/API/Element
//// Used by Document’s createElement() method.
////@TODO maybe clientHeight, clientLeft, clientTop, clientWidth
////@TODO get and set `id` and `className` are done, but need to do more like that
////@TODO setter for outerHTML,
////@TODO closest(), getAttributeNames(), getAttributeNode()
////@TODO maybe getBoundingClientRec(), getClientRects
////@TODO getElementsByClassName(), getElementsByTagName(), hasAttributes()
////@TODO insertAdjacentElement(), insertAdjacentHTML(), insertAdjacentText()
////@TODO matches(), querySelectorAll(), removeAttributeNode(), setAttributeNode()
////@TODO toggleAttribute()
class Element extends Node {
    constructor () {
        super()

        this.attributes = [] //@TODO should be a NamedNodeMap
        this.classList = new DOMTokenList()
        this.dataset = {} //@TODO should be private
        this.style = new CSSStyleDeclaration(this)

        //// Allow $myElement.id = 'my-element', etc, and make them enumerable.
        //@TODO more of these
        Object.defineProperty(this, 'className', {
            enumerable: true
          , get () { return this.getAttribute('class') } // note 'class'
          , set (v) { return this.setAttribute('class', v) } // note 'class'
        })
        Object.defineProperty(this, 'id', {
            enumerable: true
          , get () { return this.getAttribute('id') }
          , set (v) { return this.setAttribute('id', v) }
        })

    }

    get nodeType () { return 1 }
    get nodeName () { return this.tagName }

    //// developer.mozilla.org/en-US/docs/Web/API/Element/innerHTML
    get innerHTML () {
        let s = '' // regurgitate set innerHTML
        this.childNodes.forEach( e => s += (e.outerHTML || e.textContent) )
        if (revealCalls) console.log('get innerHTML of', this.nodeName, s);
        return s
    }
    set innerHTML (v) {
        parseIntoChildNodes(this, v)
        if (revealCalls) console.log('set innerHTML of', this.nodeName, this.childNodes.length, `'${v}'`, this.innerHTML)
    }

    //// developer.mozilla.org/en-US/docs/Web/API/Element/outerHTML
    get outerHTML () {
        // return '<article><my-component></my-component></article>'
        const
            a = []
          , VOID = { AREA:1, BASE:1, BR:1, COL:1, EMBED:1, HR:1, IMG:1, INPUT:1
              , KEYGEN:1, LINK:1, META:1, PARAM:1, SOURCE:1, TRACK:1, WBR:1 }
          , attrs = this.style.cssText
              ? this.attributes.concat( [{ name:'style' }] )
              : this.attributes
          , p = propertify(this)
          , s = stringify(attrs)
          , d = dataify(this.dataset)

        a.push('<' + this.nodeName + p + s + d + '>')
        if (! VOID[this.nodeName.toUpperCase()] ) {
            a.push(this.innerHTML)
            a.push('</'+this.nodeName+'>')
        }

        if (revealCalls) console.log('get outerHTML of', this.nodeName, a.join(''))
        return a.join('')

        function stringify (arr) {
            let attr = [], value
            arr.forEach( a => {
                value = ('style' === a.name) ? this.style.cssText : a.value
                attr.push(a.name+'="'+escapeAttribute(value)+'"')
            })
            return attr.length ? ' '+attr.join(' ') : '';
        }

        function dataify (data) {
            let attr = [], value;
            Object.keys(data).forEach( name =>
                attr.push('data-'+name+'="'+escapeAttribute(data[name])+'"') )
            return attr.length ? ' '+attr.join(' ') : '';
        }

        function propertify (self) {
            let props = []
            for (let key in self) {
                const attrName = propToAttr(key)
                if (
                    self.hasOwnProperty(key)
                 && -1 < ['string','boolean','number'].indexOf(typeof self[key])
                 && isStandardAttribute(attrName, self.nodeName)
                 && shouldOutputProp(key, attrName, self)
                ) {
                    props.push({ name:attrName, value:self[key] })
                }
            }
            return props ? stringify(props) : ''
        }

        function shouldOutputProp(prop, attrName, self) {
            if ( self.getAttribute(attrName) )
                return false // let explicitly-set attributes override props
            if ('className' === prop  && ! self[prop])
                return false
            return true
        }

    }

    //// developer.mozilla.org/en-US/docs/Web/API/Element/getAttribute
    getAttribute (n) {
        if (revealCalls) console.log(this.nodeName, 'getAttribute', n)
        if ('style' === n) return this.style.cssText
        const result = this._getProperty(this.attributes, n)
        return 'undefined' === typeof result ? null : result.value
    }

    //// developer.mozilla.org/en-US/docs/Web/API/Element/hasAttribute
    hasAttribute (n) {
        const result = this._getProperty(this.attributes, n)
        if (revealCalls) console.log(this.nodeName, 'hasAttribute', n, 'undefined' !== typeof result)
        return 'undefined' !== typeof result
    }

    //// developer.mozilla.org/en-US/docs/Web/API/Element/querySelector
    querySelector (selectors) {
        if (revealCalls) console.log(this.nodeName, 'Element.querySelector', selectors)
        //@TODO implement
    }

    //// developer.mozilla.org/en-US/docs/Web/API/Element/removeAttribute
    removeAttribute (n) {
        if (revealCalls) console.log(this.nodeName, 'removeAttribute', n)
        if (n === 'class') return this.className = ''
        for (let i=0, l=this.attributes.length; i<l; i++) {
            if (this.attributes[i].name === n) {
                this.attributes.splice(i, 1)
                break
            }
        }
    }

    //// developer.mozilla.org/en-US/docs/Web/API/Element/setAttribute
    setAttribute (n, v) {
        if (revealCalls) console.log(this.nodeName, 'setAttribute', n, v)
        if ('style' === n) return this.style.cssText = v
        this._setProperty(this.attributes, Attribute, n, v)
    }

    //// Get something like { name:'balloons', value:'99' } from a given array.
    _getProperty (arr, key) {
        if (! key) return
        key = key.toLowerCase()
        for (let i=0; i<arr.length; i++)
            if (key == arr[i].name) return arr[i]
    }

    //// Add something like { name:'color', value:'red' } to a given array.
    _setProperty (arr, obj, key, val) {
        const p = this._getProperty(arr, key)
        if (p) return p.value = val+''
        arr.push(
            'function' === typeof obj
          ? new obj( key.toLowerCase(), val+'' )
          : obj
        )
    }

}




//// DOCUMENT

//// developer.mozilla.org/en-US/docs/Web/API/Document
//// Used by Dumdom’s constructor() to make its `document` property.
////@TODO lots of properties and methods
class Document extends Node {
    constructor () {
        super()
        this.body = this.createElement('body')
        this.documentElement = this.createElement('html')
    }
    get nodeType () { return 9 }
    get nodeName () { return '#document' }

    //// developer.mozilla.org/en-US/docs/Web/API/Document/createComment
    createComment (data) {
        if (revealCalls) console.log('createComment', `'${data}'`)
        const c = new Comment()
        c.data = data
        return c
    }

    //// developer.mozilla.org/en-US/docs/Web/API/Document/createElement
    createElement (nodeName) {
        if (revealCalls) console.log('createElement', nodeName)
        const el = new Element()
        el.tagName = nodeName
        return el
    }

    //// developer.mozilla.org/en-US/docs/Web/API/Document/createTextNode
    createTextNode (data) {
        if (revealCalls) console.log('createTextNode', `'${data}'`)
        var t = new TextNode()
        t.textContent = data
        return t
    }

}




//// DUMDOM

//// An instance of Dumdom represents a simplified virtual DOM.
class Dumdom {
    constructor () {

        //// Polyfill `window`.
        this.window = {
            navigator: { userAgent: 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10.11; rv:63.0) Gecko/20100101 Firefox/63.0"' } // same as my dev browser
          , MutationObserver //@TODO remove if not used
        }

        //// Polyfill `document`.
        this.document = new Document()
        this.window.document = this.document

        //// developer.mozilla.org/en-US/docs/Web/API/Document/querySelector
        this.document.querySelector = function (selectors) {
            if (revealCalls) console.log('document.querySelector', selectors)
            if ('body' === selectors)
                return this.body // simplest case
            for (let i=0; i<this.body.childNodes.length; i++)
                if (selectors === this.body.childNodes[i].tagName)
                    return this.body.childNodes[i] // very simple case
            throw Error(`Don’t know that one! '${selectors}'`)
        }
    }
}

//// Global shim for Node.js.
if ('object' !== typeof window) {
    const dumdom = new Dumdom()
    global.window = dumdom.window
    global.document = dumdom.document
}

//// Importers might like access to the Dumdom class.
export default Dumdom
