//// Global `process` shim for browsers.
if ('object' !== typeof process || ! process.stdout) {
    if ('object' !== typeof window) throw Error('Eh? What is this runtime?!')
    window.process = {
        exit (code) { this._hasExited = true; this._exitCode = code }
      , stdout: {
            write (s) {}
        }
      , stdin: {
            setRawMode (mode) {}
          , resume () {}
          , setEncoding (encoding) {}
          , _dataCallbacks: []
          , on (eventName, callback) {
                if ('data' === eventName) this._dataCallbacks.push(callback)
            }
        }
    }
}




//// Simulates a terminal, when running Dumcli in a browser.
class Terminal {
    constructor ($output, W=79, H=24) {
        this.$output = $output
        this.W = W, this.H = H // dimensions
        this._x = this._y = 0 // cursor position
        this.showCursor = true
        this.clear()
    }

    get x ()  { return this._x }
    set x (v) { return this._x = Math.max( 0, Math.min(v, this.W-1) ) }
    get y ()  { return this._y }
    set y (v) { return this._y = Math.max( 0, Math.min(v, this.H-1) ) }

    clear () {
        this.rows = []
        for (let y=0; y<this.H; y++) {
            const row = []
            this.rows.push(row)
            for (let x=0; x<this.W; x++)
                row.push(' ')
        }
        this.render()
    }

    write (msg) {
        for (let i=0; i<msg.length; i++) {
            if ('\x1B[' === msg.substr(i, 2)) {
                if ( '\x1B[0;0H' === msg.substr(i, 6) ) { // position cursor top left
                    this.x = this.y = 0
                    i += 5
                } else if ( '\x1B[K' === msg.substr(i, 3) ) { // erase to end of row
                    for (let j=this.x; j<this.W; j++)
                        this.rows[this.y][j] = ' '
                    i += 2
                } else if (/^\x1B\[\dD$/.test(msg.substr(i, 4))) { // move cursor back 0 to 9
                    this.x -= msg.substr(i, 4).match(/^\x1B\[(\d)D$/)[1]
                    i += 3
                } else if (/^\x1B\[\d\dD$/.test(msg.substr(i, 5))) { // move cursor back 10 to 99
                    this.x -= msg.substr(i, 5).match(/^\x1B\[(\d\d)D$/)[1]
                    i += 4
                } else if (/^\x1B\[\d\d\dD$/.test(msg.substr(i, 6))) { // move cursor back 100 to 999
                    this.x = this.x - msg.substr(i, 6).match(/^\x1B\[(\d\d\d)D$/)[1]
                    i += 5
                } else if (/^\x1B\[\dm.\x1B\[\d\dm$/.test(msg.substr(i, 10))) { // single character inline formatting
                    const [ _, pfxCode, char, sfxCode ] = msg.substr(i, 10).match(/^\x1B\[(\d)m(.)\x1B\[(\d\d)m$/)
                    const pfxTag = { 1:'b', 2:'b', 3:'em', 7:'i', 9:'s', 4:'u' }[pfxCode]
                    const sfxTag = { 22:'b', 23:'em', 27:'i', 29:'s', 24:'u' }[sfxCode]
                    if (pfxTag !== sfxTag) throw Error(`Prefix code '${pfxCode}' does not match suffix code '${sfxCode}'`)
                    const cls = '2' === pfxCode ? ' class="dumcli-faint"' : ''
                    this.rows[this.y][this.x] = '<'+ pfxTag + cls + '>' + char + '</' + sfxTag + '>' //@TODO refactor write() in case `char` should be an HTML entity or rendered as '?'
                    i += 9
                    this.x++
                } else if (/^(\x1B\[\dm)+./.test(msg.substr(i))) { // begin inline formatting
                    const [ all, pfxCode1, char ] = msg.substr(i).match(/^(\x1B\[\dm)+(.)/)
                    const pfxCodes = all.slice(2,-2).split('m\x1B[')
// console.log('pfxCodes',pfxCodes, msg.substr(i).match(/^(\x1B\[\dm)+(.)/))
                    let pfxTags = ''
                    for (let pfxCode of pfxCodes) {
                        const pfxTag = { 1:'b', 2:'b', 3:'em', 7:'i', 9:'s', 4:'u' }[pfxCode]
                        pfxTags += '<' + pfxTag + ('2' === pfxCode ? ' class="dumcli-faint"' : '') + '>'
                        i += 4
                    }
                    this.rows[this.y][this.x] = pfxTags + char //@TODO refactor write() in case `char` should be an HTML entity or rendered as '?'
                    this.x++
                } else {
console.warn('eh?!', this.x, this.y, msg[i-1] + ' ' + '\\x' + msg.charCodeAt(i).toString(16) + ' ' + msg[i+1] + ' ' + msg[i+2] + ' ' + msg[i+3] + ' ' + msg[i+4])
                }
            } else if (/^.(\x1B\[\d\dm)+/.test(msg.substr(i))) { // end inline formatting
                const [ all, char, sfxCode1 ] = msg.substr(i).match(/^(.)(\x1B\[\d\dm)+/)
                const sfxCodes = all.slice(3,-1).split('m\x1B[').reverse()
// console.log('sfxCodes',sfxCodes, msg.substr(i).match(/^(.)(\x1B\[\d\dm)+/))
                let sfxTags = ''
                for (let sfxCode of sfxCodes) {
                    const sfxTag = { 22:'b', 23:'em', 27:'i', 29:'s', 24:'u' }[sfxCode]
                    sfxTags += '</' + sfxTag + '>'
                    i += 5
                }
                this.rows[this.y][this.x] = char + sfxTags //@TODO refactor write() in case `char` should be an HTML entity or rendered as '?'
                this.x++
            } else if ('\n' === msg[i]) { // newline
                this.x = 0; this.y++
            } else if ('<' === msg[i]) { // formatting uses ansi codes, not HTML markup
                this.rows[this.y][this.x] = '&lt;'
                this.x++
            } else if (/^\u00A0$/.test(msg[i])) { // NO-BREAK SPACE @TODO test
                this.rows[this.y][this.x] = '&nbsp;'
                this.x++
            } else if (/^\u007F$|^\u00AD$/.test(msg[i])) { // DEL or SHY @TODO maybe actually do a delete? Also, apply soft hyphens when Dumcli’s hyphenates
                this.rows[this.y][this.x] = '?'
                this.x++
            } else if (/^[\x20-\x7E]$/.test(msg[i])) { // printable ascii from 0x20 (space) to 0x7E (tilde)
                this.rows[this.y][this.x] = msg[i]
                this.x++
            } else if (/^[\x00-\x1F]$/.test(msg[i])) { // low-ascii control character
                this.rows[this.y][this.x] = '?'
                this.x++
            } else if (/^[\u0080-\u009F]$/.test(msg[i])) { // UTF-16 control character inbetween DEL and NO-BREAK SPACE
                this.rows[this.y][this.x] = '?'
                this.x++
            } else if (/^[\u0080-\u04FF]$/.test(msg[i])) { // printable UTF-16 character from '¡' to 'ӿ'
                this.rows[this.y][this.x] = '&#x' + msg.charCodeAt(i).toString(16) + ';'
                this.x++
            } else { // printable or unprintable UTF-16 character above 'ӿ' @TODO deal with these
                this.rows[this.y][this.x] = '!'
                this.x++
                // console.log('BAD!', this.x, this.y, '\x1B[K' === msg.substr(i, 3), "'"+msg.substr(i-1, 10)+"'", msg.charCodeAt(i).toString(16), msg[i+1], msg[i+2])
                console.log('NOT HANDLED YET!', this.x, this.y, '\x1B[K' === msg.substr(i, 3), msg.charCodeAt(i).toString(16) )
            }
        }
        if (this.showCursor)
            this.rows[this.y][this.x] = `<u class="dumcli-cursor">${this.rows[this.y][this.x]}</u>`
        this.render()
    }

    render () {
        this.$output.innerHTML =
            this.rows.map( row => row.join('') ).join('\n')
        // console.log(this.rows.map( row => row.join('') ).join('\n'));
    }
}




//// An array with a clamped pointer.
class List {
    constructor (list=[]) {
        this._pos = 0
        this._list = list
    }
    get length () { return this._list.length }

    get pos () { return this._pos }
    set pos (v) { return this._pos = Math.max( 0, Math.min(v, this._list.length-1) ) }

    get current () { return this._list[this._pos] }

    join (separator) {
        return this._list.join(separator)
    }
    slice (...args) {
        return this._list.slice.apply(this._list, args)
    }
    splice (...args) {
        return this._list.splice.apply(this._list, args)
    }
}




//// Xx.
class History extends List {
    constructor () {
        super()
        this.addCommand()
    }

    addCommand () {
        this._pos = this._list.length
        this._list[this._pos] = new Command()
    }

    //// Replaces the most recent command with a clone of some other command.
    replaceLast (command) {
        this._list[this._list.length-1] = new Command( command.slice(0) )
    }
}




//// Xx.
class Command extends List {
    get pos () { return this._pos } // we override `set pos`, so we must also restate `get pos`
    set pos (v) { return this._pos = Math.max( 0, Math.min(v, this._list.length) ) } // note no `-1`
    isSpace  (pos) { return ' ' === this._list[pos] }
    notSpace (pos) { return ' ' !== this._list[pos] }
    char (pos) { return this._list[pos] }
    insert (chunk) {
        if (!chunk) console.log('no chunk!');
        if ('Unidentified' === chunk) chunk = '*' // eg mobile devices `event.key`
        this.splice(this._pos, 0
          , /^[\x20-\x7E]$/.test(chunk) ? chunk : '#' ) // use `revealChunk(chunk)` to debug
        this.pos++
    }
}

//// For debugging Command @TODO delete
function revealChunk (chunk) {
    const out = []
    for (let i=0; i<chunk.length; i++)
        out.push( chunk.charCodeAt(i).toString(16) )
    return out.join('-')
}




//// Xx.
class Dumcli {

    constructor (appState, prompt='~ ') {

        //// Record config.
        this._appState = appState
        this._prompt = prompt

        //// Initialise dumcli command-history.
        this._history = new History()

        //// Get chunks on a keystroke-by-keystroke basis.
        //// Begin reading from stdin so the process does not exit.
        //// Expect regular text, not binary.
        process.stdin.setRawMode(true)
        process.stdin.resume()
        process.stdin.setEncoding('utf8')

        //// Xx.
        process.stdin.on('data', chunk => {
            if (process._hasExited) return //@TODO better way of quitting for browser
            const { autocomplete, _appState, _history } = this
            const command = _history.current
            const focus = _appState.cliFocus
            const special = focus ? this.cliSpecials[chunk] : this.guiSpecials[chunk]
            if (special)
                special(command, _history, _appState, autocomplete)
            else if (focus)
                command.insert(chunk)
            else
                if (0) console.log('GUI', chunk)
            this.render()
// this._appState.top = '<b>' + command.length + '</b>'
// this._appState.top = command.length + ' ' + command.pos + ' ' + 0 + ' ' + chunk + ' ' +  command._list.join('-')
// process.stdout.write(chunk.charCodeAt(0).toString(16) + ' ' + chunk.charCodeAt(1).toString(16) + ' ' + chunk.charCodeAt(2).toString(16) + ' ' + chunk.charCodeAt(3).toString(16) + ' ' + chunk.charCodeAt(4).toString(16) + ' ' + chunk.charCodeAt(5).toString(16) + ' - ')
        })
    }

    //// Used by tryAutocomplete().
    get autocomplete () { return [
        'exit', 'help'
    ]}

    //// Define special keystrokes for the Dumbox ‘graphical user interface’.
    get guiSpecials () {
        if (this._guiSpecials) return this._guiSpecials // use memoised...
        const specials = this._guiSpecials = { // ...or if not, make them

            //// App functionality.
            '\u0003': onExit

            //// Whitespace
          , '\u001b': onEscape // ctrl-[ or the escape key

            //// Arrows.
          , '\u0010': onScrollLayoutUp   // ctrl-p up arrow (previous command)
          , '\u000e': onScrollLayoutDown // ctrl-n down arrow (next command)
        }

        //// Add some keyboard aliases to `specials`.
        specials['\u000d'] = specials['\u000a'] // enter or return key
        specials['\u001b\u005b\u0041'] = specials['\u0010'] // up arrow key
        specials['\u001b\u005b\u0042'] = specials['\u000e'] // down arrow key
        // specials['\u001b\u005b\u0043'] = specials['\u0006'] // right arrow key
        // specials['\u001b\u005b\u0044'] = specials['\u0002'] // left arrow key

        return specials
    }

    //// Define special keystrokes for the command line.
    get cliSpecials () {
        if (this._cliSpecials) return this._cliSpecials // use memoised...
        const specials = this._cliSpecials = { // ...or if not, make them

            //// App functionality.
            '\u0003': onExit

            //// Whitespace
          , '\u000a': onEnter // ctrl-j newline - treated as enter
          , '\u001b': onEscape // ctrl-[ or the escape key
          , '\u0009': tryAutocomplete // ctrl-i or the tab key
          , '\u001b\u005b\u005a': c => c.insert('T') // shift-tab

            //// Delete.
          , '\u0015': c => { c.splice(0, c.pos); c.pos = 0 } // ctrl-u delete from cursor to start
          , '\u000b': c => c.splice(c.pos, c.length-c.pos) // ctrl-k delete from cursor to end
          , '\u0017': deletePreviousWord // ctrl-w delete previous word
          , '\u0008': c => { if (c.pos) c.splice(--c.pos, 1) } // ctrl-h backspace (delete to the left)
          , '\u0004': c => c.splice(c.pos, 1) // ctrl-d delete (delete to the right)

            //// Arrows and moving the cursor.
          , '\u0010': (c, h) => { c.pos = c.length; h.pos-- } // ctrl-p up arrow (previous command)
          , '\u000e': (c, h) => { c.pos = c.length; h.pos++ } // ctrl-n down arrow (next command)
          , '\u0002': c => c.pos-- // ctrl-b left arrow (back)
          , '\u0006': c => c.pos++ // ctrl-f right (forward)
          , '\u0001': c => c.pos = 0 // ctrl-a to start of line (home)
          , '\u0005': c => c.pos = c.length // ctrl-e to end of line
          , '\u001b\u0066': moveToWordEnd // alt-f to end of word*
          , '\u001b\u0062': moveToWordStart // alt-b to start of word*
        } //* also triggered by alt-left-arrow-key and alt-right-arrow-key on macOS

        //// Add some keyboard aliases to `specials`.
        specials['\u000d'] = specials['\u000a'] // enter or return key
        specials['\u007f'] = specials['\u0008'] // delete key
        specials['\u001b\u005b\u0033\u007e'] = specials['\u0004'] // forward delete key
        specials['\u001b\u005b\u0041'] = specials['\u0010'] // up arrow key
        specials['\u001b\u005b\u0042'] = specials['\u000e'] // down arrow key
        specials['\u001b\u005b\u0043'] = specials['\u0006'] // right arrow key
        specials['\u001b\u005b\u0044'] = specials['\u0002'] // left arrow key

        return specials
    }

    get shiftKeypressToCtrl () {
        return {
            Tab: '\u001b\u005b\u005a' // shift-tab
        }
    }

    get ctrlKeypressToCtrl () {
        return {
            j: '\u000a' // ctrl-j newline - treated as enter
          ,'[':'\u001b' // ctrl-[ or the escape key
          , i: '\u0009' // ctrl-i or the tab key

          , u: '\u0015' // ctrl-u delete from cursor to start
          , k: '\u000b' // ctrl-k delete from cursor to end
          , w: '\u0017' // ctrl-w delete previous word
          , h: '\u0008' // ctrl-h backspace (delete to the left)
          , d: '\u0004' // ctrl-d delete (delete to the right)

          , p: '\u0010' // ctrl-p up arrow (previous command)
          , n: '\u000e' // ctrl-n down arrow (next command)
          , b: '\u0002' // ctrl-b left arrow (back)
          , f: '\u0006' // ctrl-f right (forward)
          , a: '\u0001' // ctrl-a to start of line (home)
          , e: '\u0005' // ctrl-e to end of line
        }
    }

    get altKeypressToCtrl () {
        return {
            ArrowRight: '\u001b\u0066' // alt-f to end of word*
          , ArrowLeft:  '\u001b\u0062' // alt-b to start of word*
        }
    }

    get keypressToCtrl () {
        return {
            Enter:      '\u000a' // ctrl-j newline - treated as enter
          , Escape:     '\u001b' // ctrl-[ or the escape key
          , Tab:        '\u0009' // ctrl-i or the tab key
          , Backspace:  '\u0008' // ctrl-h backspace (delete to the left)
          , Delete:     '\u0004' // ctrl-d delete (delete to the right)
          , ArrowUp:    '\u0010' // ctrl-p up arrow (previous command)
          , ArrowDown:  '\u000e' // ctrl-n down arrow (next command)
          , ArrowLeft:  '\u0002' // ctrl-b left arrow (back)
          , ArrowRight: '\u0006' // ctrl-f right (forward)
        }
    }

    //// In the browser, set up the #dumcli-input element.
    ////@TODO support Android Chrome, which does not implement 'keydown' correctly
    setUpBrowserInput ($input) {
        $input.addEventListener('keydown', evt => {
            let key = evt.key

            //// Generic keypress housekeeping.
            if (! key) key = String.fromCharCode(event.which || event.code) // if `event.key` is not supported @TODO test
            if (/^Alt$|^Control$|^Meta$|^Shift$/.test(key)) return // ignore just Alt etc
            if (evt.metaKey) return // allow most browser shortcuts to work
            evt.preventDefault()

            ////@TODO deal with eg ctrl+alt together
            if ( /^[\x20-\x7E]$/.test(key) ) {
                if (evt.ctrlKey && this.ctrlKeypressToCtrl[key])
                    key = this.ctrlKeypressToCtrl[key]
            } else {
                if (evt.shiftKey && this.shiftKeypressToCtrl[key])
                    key = this.shiftKeypressToCtrl[key]
                else if (evt.altKey && this.altKeypressToCtrl[key])
                    key = this.altKeypressToCtrl[key]
                else if (this.keypressToCtrl[key])
                    key = this.keypressToCtrl[key]
            }
            window.process.stdin._dataCallbacks.forEach( cb => cb(key) )

        })
    }

    //// In the browser, set up the '#dumcli pre' element.
    setUpBrowserOutput ($output, W, H) {
        const terminal = this._terminal = new Terminal($output, W, H)
        window.process.stdout.write = function (msg) {
            terminal.write(msg)
        }
    }

    render () {
        const { _history, _prompt, _appState } = this
        const command = _history.current
        const focus = _appState.cliFocus
        if (process._hasExited) {
            this._terminal.showCursor = false
            process.stdout.write(
                '\x1B[999D\x1B[K' // move cursor to start of line and erase line
              + 'exit code: ' + process._exitCode // show an exit message
            )
        } else {
            process.stdout.write(
                '\x1B[999D' // move cursor to start of line
              + (focus ? '' : '\x1B[2m') // faint
              + _prompt
              + '\x1B[K' // erase to end of line
              + command.join('') // re-write the command line
              + ' ' // and add a space on the end for the cursor to sit in
              + (focus ? '' : '\x1B[22m') // end faint
              + '\x1B[' + (command.length-command.pos+1) + 'D' // move cursor to its proper position
            )
        }
    }

}




//// UTILITY

function onExit () {
    process.stdout.write('\nctrl-c (exit)\n')
    process.exit(0)
}

//// Xx.
function onEnter (command, history, appState) {

    //// Ignore whitespace commands.
    const str = command.join('').trim()
    if ('' === str) return

    //// Move cursor to the end of the line.
    command.pos = command.length

    ////@TODO execute the command properly
    if ('help' === str)
        appState.top = 'Help is coming soon!'
    else if ('exit' === str)
        appState.top = "You typed 'exit' - see you later!" //@TODO make this show up on the terminal
      , process.stdout.write('\n'), process.exit(0)
    else
        appState.top = `${str}: command not found`

    //// If we are in the middle of history, replace the most recent command
    //// with a clone of the old command.
    if (history.pos !== history.length-1) history.replaceLast(command)

    //// Create a new blank command at the end of history.
    history.addCommand()
}

//// Xx.
function onEscape (command, history, appState) {
    appState.toggleCliFocus()
}

function tryAutocomplete (c, history, appState, autocomplete) {

    //// Get the partial-word which the cursor is currently in.
    let before = '', after = '', partWord = ''
    for (let i=c.pos; i>0 && c.notSpace(i-1); i--)
        before = c.char(i-1) + before
    for (let i=c.pos; i<c.length && c.notSpace(i); i++)
        after = after + c.char(i)
    partWord = before + after
    if ('' === partWord) return // not in a word (cursor must be in 2+ spaces)

    //// Find the autocomplete word. Must be exactly one for autocompletion.
    const candidates = []
    for (let candidate of autocomplete)
        if ( candidate.substr(0, partWord.length) === partWord )
            candidates.push(candidate)
    if (1 !== candidates.length) return

    //// Insert the missing part of the word.
    c.splice.apply(
        c // the `this` argument
      , [   c.pos - before.length // start at the start of the partial-word
          , partWord.length // remove the partial-word
        ].concat( candidates[0].split('') ) // each letter is an array item
    )
    c.pos = c.pos - before.length + candidates[0].length
}

function deletePreviousWord (c) {
    if (c.isSpace(c.pos-1)) deleteBackTilWord(c)
    deleteBackTilSpace(c)
}
function deleteBackTilWord (c) {
    if (! c.pos || c.notSpace(c.pos-1)) return
    c.splice(--c.pos, 1)
    deleteBackTilWord(c)
}
function deleteBackTilSpace (c) {
    if (! c.pos || c.isSpace(c.pos-1)) return
    c.splice(--c.pos, 1)
    deleteBackTilSpace(c)
}

function moveToWordStart (c) {
    if (0 === c.pos) return
    if (c.isSpace(c.pos-1)) moveBackTilWord(c)
    moveBackTilSpace(c)
}
function moveBackTilWord (c) {
    for (; c.pos>0; c.pos--)
        if (c.notSpace(c.pos-1)) return
}
function moveBackTilSpace (c) {
    for (; c.pos>0; c.pos--)
        if (c.isSpace(c.pos-1)) return
}

function moveToWordEnd (c) {
    if (c.length === c.pos) return
    if (c.isSpace(c.pos+1)) moveForwardTilWord(c)
    moveForwardTilSpace(c)
    c.pos++
}
function moveForwardTilWord (c) {
    for (; c.pos<c.length-1; c.pos++)
        if (c.notSpace(c.pos+1)) return
}
function moveForwardTilSpace (c) {
    for (; c.pos<c.length-1; c.pos++)
        if (c.isSpace(c.pos+1)) return
}

////
function onScrollLayoutUp (c, h, appState) {
    // appState.top = 'Y: ' + appState.layoutScrollY
    appState.scrollLayout(-1)
}
function onScrollLayoutDown (c, h, appState) {
    // appState.top = 'Y: ' + appState.layoutScrollY
    appState.scrollLayout(1)
}

//// Xx.
export default Dumcli
