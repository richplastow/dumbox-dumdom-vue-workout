//// A 2D array of characters.
class Screen {
    constructor (W=79, H=24) {
        this.W = W
        this.H = H
        this._rows = []
        for (let y=0; y<H; y++) {
            const row = []
            this._rows.push(row)
            for (let x=0; x<W; x++)
                row.push(' ')
        }
    }

    toString () {
        return this._rows.map( row => row.join('') ).join('\n')
    }

    set (char, x, y) {
        const { W, H, _rows } = this
        if (0 > x || W <= x) return
        if (0 > y || H <= y) return
        _rows[y][x] = char
    }
}




//// Line

//// A line of text in an Text element.
class Line {
    constructor () {
        this._chars = []
    }

    get count () {
        return this._chars.length
    }

    add (word) {
        this._chars = this._chars.concat( this.ansify(word) )
    }

    charAt (i) {
        return this._chars[i]
    }

    //// Wraps words in ANSI codes, and converts them to an array.
    ansify (word) {
        const
            pfxCodes = []
          , sfxCodes = []
          , cs = (
                (word.leadingSpaces ? ' ' : '')
              + word.chars
              + (word.trailingSpaces ? ' ' : '')
            ).split('')

        //// Build the prefix and suffix strings.
        for (let tag of word.tags) {
            tag = tag.toLowerCase()
            const pfxCode = { a: 4, b: 1, em: 3, i: 7, s: 9, strong: 1, small: 2, u: 4 }[tag] //@TODO <small> is '2' (faint)
            const sfxCode = { a:24, b:22, em:23, i:27, s:29, strong:22, small:22, u:24 }[tag]
            if (! pfxCode) continue // tag not recognised, eg 'span'
            pfxCodes.push(pfxCode)
            sfxCodes.push(sfxCode)
        }
        //@TODO remove dupes
        const pfx = `\x1B[${pfxCodes.join('m\x1B[')}m` // prefix
        const sfx = `\x1B[${sfxCodes.join('m\x1B[')}m` // suffix

        //// Deal with simple cases.
        if (! pfxCodes.length) return cs // nodeName is '#text', or some unrecognised value
        if (! cs.length) return []
        if (1 === cs.length) return [ pfx + cs[0] + sfx ] // 1-char word

        //// For 2+ characters, start with the middle characters and then add
        //// the formatted first and last chars.
        return [ pfx+cs[0] ].concat( cs.slice(1,-1), cs.slice(-1)+sfx )
    }

}




//// EL

//// An easily renderable representation of an HTML element. Not used directly!
class El {
    constructor ($el, W, H=0) {
        this.W = W
        this.H = H
    }
}




//// WORD

//// Used by Text to represent a single word.
class Word {
    constructor (chars, tags=[]) {
        this.chars = chars
        this.tags = tags // an array of inline nodeNames which apply to this word
        this.leadingSpaces = 0 // only the first word of an $el can have this
        this.trailingSpaces = 1 // only the last word of an $el may not have this
    }

    //// The character-length, plus leading and trailing spaces, if any.
    get count () {
        return this.leadingSpaces + this.chars.length + this.trailingSpaces
    }
}




//// TEXT

//// An easily renderable representation of an HTML inline Element (eg <B>), or
//// an HTML TextNode. Dumbox’s Text class merges both concepts into one.
class Text extends El {
    constructor ($el, W) {
        super($el, W)
        this.tagStack = []
        this.words = [] // eg [{ chars:'emphasised', tag:'em' }, ...]

        ////
        if (1 === $el.nodeType)
            this.appendInlineElement($el)
        else
            this.appendTextNode($el)

        //// Init the array of Line objects, used by buildContent().
        this.lines = [ new Line() ]
    }

    //// Xx.
    appendInlineElement ($el) {
// console.log( '.'.repeat(40) + "appendInlineElement() <" + $el.nodeName.toLowerCase() + ">", this.tagStack.slice(0).join(' ') )

        //// Any Word found inside $el should have the current set of tag-names
        //// applied to it, along with this $el’s tag-name.
        this.tagStack.push( $el.nodeName.toLowerCase() )

        //// Step through each child-node.
        for (let $subel of $el.childNodes) {
            let subel, subelType
              , nodeNameLC = $subel.nodeName.toLowerCase()

            //// Respect the special 'dumbox-ignore' class.
            if ( $subel.classList && $subel.classList.contains('dumbox-ignore') )
                continue

            //// Determine the type of HTML Node: 'text', 'inline' or 'block'.
            if (3 === $subel.nodeType)
                subelType = 'text'
            else if (1 === $subel.nodeType)
                subelType = blockInfo($subel) ? 'block' : 'inline'
            else
                continue // ignore comments etc

            //// Block HTML Elements are not allowed inside inline Elements.
            if ('br' === nodeNameLC)
                this.appendNewline()
            else if ('block' === subelType)
                throw Error(`Found block <${$subel.nodeName}> in inline <${$el.nodeName}>`) //@TODO support it?

            //// Deal with a plain HTML TextNode.
            if ('text' === subelType)
                if (! $subel.textContent.trim() )
                    continue // ignore empty TextNodes @TODO maybe allow an inline Element containing only whitespace?
                else
                    this.appendTextNode($subel)

            //// Deal with an inline HTML Element (nested in another inline).
            if ('inline' === subelType)
                this.appendInlineElement($subel)
        }

        //// Words appended after $el will not be effected by its tag-name.
        this.tagStack.pop()
    }

    //// Xx.
    appendNewline () {
        this.words.push(new Word('\n'))
    }

    //// Converts an HTML TextNode’s `textContent` to a sequence of Words,
    //// and appends these Words to the `this.words` array.
    appendTextNode ($text) {
// console.log( '.'.repeat(40) + "appendTextNode() '" + $text.textContent + "'", this.tagStack.slice(0).join(' ') )
        const { W } = this

        //// Convert all whitespaces to regular spaces in `textContent`.
        //// Then convert to an array of words.
        let text = $text.textContent.replace(/\n/g,' ')
        text = text.split(' ').filter(w => w) // filter() removes double-spaces

        //// Ignore the TextNodes if it’s empty, or it just contains whitespace.
        //@TODO maybe?

        //// Hyphenate words too long to fit within the width, `W`.
        for (let i=0; i<text.length; i++)
            if (W < text[i].length + 1) // `+ 1` for the space @TODO why?
                text.splice(i, 1 // delete it from the array of words, and...
                  , text[i].slice(0, W-2) + '-' // ...replace it with 'firstpart-'
                  , text[i].slice(W-2) ) // ...'secondpart'

        //// Record each word’s `nodeName` along with its characters. This will
        //// be used by Line’s ansify() method to add formatting to each word.
        const words = []
        for (let word of text)
            words.push( new Word( word, this.tagStack.slice(0) ) ) // slice() makes a shallow copy

        //// Keep a leading space, if first word has leading whitespace. Remove
        //// the default trailing space from the last word, if none exists.
        if ( /^\s/.test($text.textContent) )
            words[0].leadingSpaces = 1
        if ($text.textContent && ! /\s$/.test($text.textContent) )
            words[words.length-1].trailingSpaces = 0

        this.words = this.words.concat(words)
    }

    //// Populates `lines` with all of the HTML TextNodes’s `textContent`, at
    //// the current width. Note that `H` (height) equals `lines.length`.
    buildContent () {
        const { W, words } = this
        if (! words.length) return this.H = 0 // `this.H` is probably 0 already

        //// Remove whitespace from the very beginning and the very end.
        words[0].leadingSpaces = 0
        words[words.length-1].trailingSpaces = 0

        //// Step through each word.
        while (words.length) {
            const l = this.lines.length - 1 // index of the current line
            if ('\n' === words[0].chars) {
                words.shift()
                this.lines.push( new Line() ) // a newline, created by <BR>
            } else if (W >= this.lines[l].count + words[0].count) {
                if (0 === this.lines[l].count)
                    words[0].leadingSpaces = 0
                this.lines[l].add( words.shift() ) // word fits, so append it to the line
            } else if (W < words[0].count) {
                throw Error(`appendTextNode() did not hyphenate ${words[0].chars}\npreventing infinite loop!`)
            } else {
                this.lines.push( new Line() ) // does not fit, so start a new line
            }
        }
        this.H = this.lines.length
    }

    render (X, Y, screen) {
        for (let l=0; l<this.lines.length; l++)
            for (let c=0; c<this.lines[l].count; c++) {
                screen.set( this.lines[l].charAt(c), X+c, Y+l)
            }
    }
}




//// BLOCK

//// An easily renderable representation of an HTML block element (eg <DIV>).
class Block extends El {
    constructor ($el, W) {
        super($el, W)
        this.els = [] // an array of Block, Inline and Text instances
        this.buildEls($el, W)
    }

    //// Creates each of this Block’s child Els (Block, Inline and Text). In
    //// doing so, the child Blocks and Inlines will do their own buildEls().
    //// The child Texts will run appendTextNode().
    buildEls ($el, W) {
        let prevel

        //// Step through each of the Block’s child-nodes.
        for (let $subel of $el.childNodes) {
            let subel, subelType
              , nodeNameLC = $subel.nodeName.toLowerCase()

            //// Respect the special 'dumbox-ignore' class.
            if ( $subel.classList && $subel.classList.contains('dumbox-ignore') )
                continue

            //// Determine the type of HTML Node: 'text', 'inline' or 'block'.
            if (3 === $subel.nodeType)
                subelType = 'text'
            else if (1 === $subel.nodeType)
                subelType = blockInfo($subel) ? 'block' : 'inline'
            else
                continue // ignore comments etc

            //// Deal with a plain HTML TextNode.
            if ('text' === subelType)
                if (prevel && prevel instanceof Text) // consecutive TextNodes @TODO is this possible?
                    prevel.appendTextNode($subel)
                else
                    subel = new Text($subel, W-2)

            //// Deal with an inline HTML Element.
            if ('inline' === subelType)
                if (prevel && prevel instanceof Text)
                    prevel.appendInlineElement($subel)
                else
                    subel = new Text($subel, W-2)

            //// Deal with a block HTML Element.
            if ('block' === subelType)
                if ('br' === nodeNameLC)
                    if (prevel instanceof Br)
                        subel = new Br($subel, W-2, 1) // two or more line-breaks
                    else
                        subel = new Br($subel, W-2) // a line-break
                else if ('hr' === nodeNameLC )
                    subel = new Hr($subel, W-2) // a horizontal-rule
                else
                    subel = new Block($subel, W-2) // a normal block Element

            //// If a new sub-element was created, record it in `els`...
            if (subel) {
                this.els.push(subel)
                prevel = subel // ...and make it the new ‘previous element’
            }
        }

        //// Run `buildContent()` on all Text elements.
        this.els.map( el => { if (el instanceof Text) el.buildContent() })

        //// Set this Block’s height - the sum of its sub-elements, plus two.
        this.H = this.els.reduce( (total, el) => total + el.H, 0 ) + 2

    }

    render (X, Y, screen) {
        const { W, H } = this
        if (! W || ! H) return // ignore if zero width or height @TODO test

        ////
        screen.set("'", X, Y+H-1), screen.set("'", X+W-1, Y+H-1)
        screen.set('.', X, Y),     screen.set('.', X+W-1, Y)
        for (let x=X+1; x<X+W-1; x++)
            screen.set('-', x, Y), screen.set('=', x, Y+H-1)
        for (let y=Y+1; y<Y+H-1; y++)
            screen.set('|', X, y), screen.set('|', X+W-1, y)

        ////
        let y = Y+1
        for (let subel of this.els) {
            subel.render(X+1, y, screen)
            y += subel.H
        }
    }
}




//// BR

//// A simple representation of an HTML line-break element (a <BR>).
//// Two or more <BR>s create an empty line.
class Br extends El {
    constructor ($el, W, H) {
        super($el, W, H) // El’s default for `this.H` is 0, which is correct
    }

    //// No need to draw anything.
    render (X, Y, screen) { }
}




//// HR

//// A simple representation of an HTML horizontal-rule element (an <HR>).
class Hr extends El {
    constructor ($el, W) {
        super($el, W)
        this.H = 1 // override El’s default, which is 0
    }

    //// Draw the horizontal-rule, which is just a row of hyphens.
    render (X, Y, screen) {
        const { W, H } = this
        if (! W || ! H) return // ignore if zero width or height @TODO test
        for (let x=X+1; x<X+W-1; x++) screen.set('-', x, Y)
    }
}




//// LAYOUT

//// An easily renderable representation of the DOM. Very similar to a Block,
//// but has a maximum-height and can show a vertical scrollbar.
class Layout extends Block {
    constructor ($rootel, W, MAX_H, appState) {
        super($rootel, W)
        this.title = $rootel.title || 'Untitled'
        this.MAX_H = MAX_H
        this._appState = appState
    }

    //// Creates each of the Layout’s child Blocks. A Layout’s immediate child
    //// nodes must all be Blocks!
    buildEls ($rootel, W) {
        let prevel

        ////
        for (let $subel of $rootel.childNodes) {
            let subel
              , nodeNameLC = $subel.nodeName.toLowerCase()

            if ( $subel.classList && $subel.classList.contains('dumbox-ignore') )
                continue
            else if (3 === $subel.nodeType && $subel.textContent.trim() ) // empty TextNodes are ignored
                throw Error(`Found text '${$subel.textContent.trim()}' in root-el’s top-level`) //@TODO support it?
            else if (1 === $subel.nodeType) // an Element
                if ('br' === nodeNameLC)
                    if (prevel instanceof Br)
                        subel = new Br($subel, W-2, 1) // two or more line-breaks
                    else
                        subel = new Br($subel, W-2) // a line-break
                else if ('hr' === nodeNameLC )
                    subel = new Hr($subel, W-2) // a horizontal-rule
                else if ( blockInfo($subel) )
                    subel = new Block($subel, W-2) // a block
                else
                    throw Error(`Inline '${$subel.nodeName}' in <BODY>’s top-level`) //@TODO support it?

            //// If a new sub-element was created, record it in `els`...
            if (subel) {
                this.els.push(subel)
                prevel = subel // ...and make it the new ‘previous element’
            }
        }

        //// Set the Layout’s CONTENT height - the sum of its sub-elements, plus two.
        this.H = this.els.reduce( (total, el) => total + el.H, 0 ) + 2
    }

    renderScrollbar (X, Y, screen) {
        const { W, H, MAX_H, _appState } = this
        const guiFocus = ! _appState.cliFocus
        const fracVisible = MAX_H / H

        //// Calculate the scrollbar height - proportional to the fraction of
        //// content currently visible.
        const scrollbarH =
            Math.max(1,         // make the scrollbar at least 1 line high
                Math.floor(     // prevent the scrollbar 100% filling the side
                    fracVisible // the fraction of content showing
                  * (MAX_H-2)   // the space for the scrollbar to fill
            ) )

        //// Calculate the scrollbar y-position.
        let scrollY = _appState.layoutScrollY
        let scrollbarY =
            Y + 1 +
            Math.ceil(      // must be an integer
                fracVisible // the fraction of content showing
              * scrollY     // the space for the scrollbar to fill
            )

        //// If the y-position would make the bottom of the scrollbar extend
        //// below the bottom of the box, clamp the app-state’s `layoutScrollY`.
        ////@TODO fix for very long content - currently, the end of `'Lorem ipsum et wisi delor consequat. '.repeat(200)` cannot be scrolled to
        if (MAX_H < scrollbarY + scrollbarH + 1) {

            //// Move the scrollbar up. @TODO until it’s valid
            _appState.layoutScrollY = Math.max(0, scrollY - 1)

            //// Recalculate the scrollbar y-position.
            scrollY = _appState.layoutScrollY
            scrollbarY =
                Y + 1 +
                Math.ceil(      // must be an integer
                    fracVisible // the fraction of content showing
                  * scrollY     // the space for the scrollbar to fill
                )
        }

        //// Draw the scrollbar over the right-edge of the Layout.
        const char = guiFocus ? '#' : '\x1B[2m#\x1B[22m'
        for (let y=scrollbarY; y<scrollbarY+scrollbarH; y++)
            screen.set(char, X+W-1, y)
    }

    render (X=0, Y=0) {
        const { W, H, MAX_H, _appState } = this
        const screen = new Screen(W, MAX_H)
        const BOX_H = Math.min(MAX_H, H) // viewport height
        if (! W || ! BOX_H) return // ignore if zero width or height @TODO test

        //// Make the Layout border faint if the CLI is focused.
        const guiFocus = ! _appState.cliFocus
        const char = {
            corner: {
                top:    guiFocus ? "." : "\x1B[2m.\x1B[22m"
              , bottom: guiFocus ? "'" : "\x1B[2m'\x1B[22m"
            }
          , border: {
                top:    guiFocus ? "-" : "\x1B[2m-\x1B[22m"
              , bottom: guiFocus ? "=" : "\x1B[2m=\x1B[22m"
              , sides:  guiFocus ? "|" : "\x1B[2m|\x1B[22m"
            }
        }

        //// Draw the corners and the left and right borders.
        screen.set(char.corner.top, X, Y)
        screen.set(char.corner.top, X+W-1, Y)
        screen.set(char.corner.bottom, X, Y+BOX_H-1)
        screen.set(char.corner.bottom, X+W-1, Y+BOX_H-1)
        for (let y=Y+1; y<Y+BOX_H-1; y++)
            screen.set(char.border.sides, X, y)
          , screen.set(char.border.sides, X+W-1, y)

        //// If the content does not fit in the viewport, overlay a scrollbar.
        //// If it does, reset the app-state’s scroll-y position (useful if the
        //// content has just shrunk and we were scrolled way down).
        if (H > BOX_H)
            this.renderScrollbar(X, Y, screen)
        else
            _appState.layoutScrollY = 0

        //// renderScrollbar() will have clamped _appState.layoutScrollY, so
        //// now we can render the Layout’s sub-elements.
        let y = Y + 1 - _appState.layoutScrollY
        for (let el of this.els) {
            el.render(X+1, y, screen)
            y += el.H
        }

        //// Overlay the top and bottom borders. @TODO a bit messy - subels should know not to draw on the first or last lines
        for (let x=X+1; x<X+W-1; x++)
            screen.set(char.border.top, x, Y)
          , screen.set(char.border.bottom, x, Y+BOX_H-1)

        return screen+''
    }
}




//// DUMBOX

//// Xx.
class Dumbox {
    constructor (appState, window, out, W=79, MAX_H=23) {
        this._appState = appState
        this.window = window
        this.document = window.document
        this.out = out
        this.W = W
        this.MAX_H = MAX_H
    }

    //// Renders the <BODY> element. Called whenever Vue updates.
    render () {
        const layout = new Layout(this.document.body, this.W, this.MAX_H, this._appState) //@TODO just edit an existing one?
        this.out.write(
            '\x1B[0;0H' // position the cursor at top left
          + '\x1B[K' // erase to end of line
          // + this.renderElement(this.document.body, 0, 0, this.W, this.H)
          + layout.render()
          + '\n'
        )
    }
}




//// UTILITY

function blockInfo ($el) {
    const nodeName = $el.nodeName.toLowerCase()
    const info = {
        'address'   : { help:'Contact information' }
      , 'article'   : { help:'Article content' }
      , 'aside'     : { help:'Aside content' }
      , 'blockquote': { help:'Long ("block") quotation' }
      , 'br'        : { help:'Line break', special:true }
      , 'details'   : { help:'Disclosure widget' }
      , 'dialog'    : { help:'Dialog box' }
      , 'dd'        : { help:'Describes a term in a description list' }
      , 'div'       : { help:'Document division' }
      , 'dl'        : { help:'Description list' }
      , 'dt'        : { help:'Description list term' }
      , 'fieldset'  : { help:'Field set label' }
      , 'figcaption': { help:'Figure caption' }
      , 'figure'    : { help:'Groups media content with a caption (see <figcaption>)' }
      , 'footer'    : { help:'Section or page footer' }
      , 'form'      : { help:'Input form' }
      , 'h1'        : { help:'Heading level 1' }
      , 'h2'        : { help:'Heading level 2' }
      , 'h3'        : { help:'Heading level 3' }
      , 'h4'        : { help:'Heading level 4' }
      , 'h5'        : { help:'Heading level 5' }
      , 'h6'        : { help:'Heading level 6' }
      , 'header'    : { help:'Section or page header' }
      , 'hgroup'    : { help:'Groups header information' }
      , 'hr'        : { help:'Horizontal rule (dividing line)' }
      , 'li'        : { help:'List item' }
      , 'main'      : { help:'Contains the central content unique to this document' }
      , 'nav'       : { help:'Contains navigation links' }
      , 'ol'        : { help:'Ordered list' }
      , 'p'         : { help:'Paragraph' }
      , 'pre'       : { help:'Preformatted text' }
      , 'section'   : { help:'Section of a web page' }
      , 'script'    : { help:'Usually a block of JavaScript', special:true }
      , 'table'     : { help:'Table' }
      , 'ul'        : { help:'Unordered list' }
    }
    return info[nodeName]
}




//// Xx.
export default Dumbox
