# Dumbox, Dumcli, Dumdom and Vue Workout

<ul>
  <li><b>Dumbox</b> renders a DOM (real or Dumdom) as ascii-art</li>
  <li><b>Dumcli</b> recreates command-line-input for the browser</li>
  <li><b>Dumdom</b> is a Vue.js-friendly minimal DOM for Node.js</li>
</ul>
<p>
Together they can make minimal Vue.js apps which run in the browser <em>and</em> the terminal.
</p>

<p>
To run this demo offline in the browser, you’ll need to fire up the
special Node server:<br>
<tt>$ cd &lt;your-absolute-path>/dumbox-dumdom-vue-workout<br>
$ npm start</tt><br>
…and then just visit <a href="http://127.0.0.1:3000/">127.0.0.1:3000/</a>
and check the output in the dark green box.
</p>
<p>
For the version hosted on GitLab Pages, visit:<br>
<a href="https://richplastow.gitlab.io/dumbox-dumdom-vue-workout/">
richplastow.gitlab.io/dumbox-dumdom-vue-workout/</a><br>
The repo is <a href="https://gitlab.com/richplastow/dumbox-dumdom-vue-workout">here</a>.
</p>
<p>
For the version on shared LAMP hosting, visit:<br>
<a href="http://loop.coop/dumbox-dumdom-vue-workout/">
loop.coop/dumbox-dumdom-vue-workout/</a><br>
Note that an <tt>.htaccess</tt> is needed to serve the proper MIME type, containing:<br>
<tt>AddType text/javascript .mjs</tt>
</p>
<p>
To run this demo in Node.js:<br>
<tt>$ cd &lt;your-absolute-path>/dumbox-dumdom-vue-workout<br>
$ npm test</tt><br>
</p>
