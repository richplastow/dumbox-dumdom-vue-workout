//// Usage:
//// $ node --experimental-modules support/test-set-innerhtml.mjs

//// If we’re in Node.js, polyfill `window` and `document` to global scope.
import '../lib/dumdom.mjs'

//// Xx.
const $article = document.createElement('article')
$article.id = 'top-level'
$article.className = 'cls1 cls2'
const $plain = document.createTextNode('Plain Text ')
const $bold = document.createElement('b')
$bold.id = 'bold-text'
$bold.className = 'cls3 cls4'
$bold.textContent = 'Bold Text '
document.body.appendChild($article)
$article.appendChild($plain)
$article.appendChild($bold)
$bold.innerHTML = 'Outer at the start<em id=thing><!-- ok -->hullo <hr><div /><i class="ok ya">There</i></em>Middle-Outer 1.'
$bold.innerHTML += 'Middle-Outer 2.<span id="plus-equaled"><u>yup</u> 123</span>Outer at the end'

console.log('\ndocument.body.innerHTML:')
console.log(document.body.innerHTML)
console.log('\nqikRender(document.body):')
console.log( qikRender(document.body).join('\n') );

function qikRender (el, out=[], indent=0) {
    out.push(
        ' '.repeat(indent)
      + (el.nodeName || '?')
      + (el.hasAttribute && el.hasAttribute('id') ? '#' + el.getAttribute('id') : '')
      + (el.hasAttribute && el.hasAttribute('class') ? '.' + el.getAttribute('class').split(' ').join('.') : '')
    )
    if (el.childNodes && '#text' !== el.nodeName)
        el.childNodes.map( subel => qikRender(subel, out, indent+2) )
    else
        out[out.length-1] += ' ' + (el.textContent)
    return out
}
