//// Just needed to serve .mjs files with text/javascript MIME type.




//// Initialize immutable server state.
const
    app = require('http').createServer(server)
  , { exec } = require('child_process')
  , port = process.env.PORT || 3000 // Heroku sets $PORT
  , dir = __dirname + '/../'

////
app.on('error', e => {
    console.error('Server error: ' + e.message)
    app.close()
})

////
app.listen( port, () => {
    console.log(`Server is listening on port ${port}`)
    exec(`open http://localhost:${port}/`)
})




//// SERVE

//// Serve the proper response.
function server (req, res) {

    //// Any GET request is a static file.
    if ('GET' === req.method)
        return serveFile(req, res)

    //// Anything else is an error.
    return error(res, 9300, 405, 'Use GET')

}


//// Serve a file.
function serveFile (req, res) {
    const { readFileSync, existsSync } = require('fs')

    //// A request for the browser start page.
    if ('/' === req.url) {
        res.writeHead(200, {'Content-Type': 'text/html'})
        res.end( readFileSync(dir + '/index.html') )
    }

    //// A request for any other HTML file.
    else if ('.html' === req.url.slice(-5) && existsSync(dir + req.url) ) {
        res.writeHead(200, {'Content-Type': 'text/html'})
        res.end( readFileSync(dir + req.url) )
    }

    //// A request for a CSS file.
    else if ('.css' === req.url.slice(-4) && existsSync(dir + req.url) ) {
        res.writeHead(200, {'Content-Type': 'text/css'})
        res.end( readFileSync(dir + req.url) )
    }

    //// A request for a standard JavaScript file.
    else if ('.js' === req.url.slice(-3) && existsSync(dir + req.url) ) {
        res.writeHead(200, {'Content-Type': 'text/javascript'})
        res.end( readFileSync(dir + req.url) )
    }

    //// A request for a file with the new JavaScript module extension.
    else if ('.mjs' === req.url.slice(-4) && existsSync(dir + req.url) ) {
        res.writeHead(200, {'Content-Type': 'text/javascript'})
        res.end( readFileSync(dir + req.url) )
    }

    //// A request for an ICO file.
    else if ('.ico' === req.url.slice(-4) && existsSync(dir + req.url) ) {
        res.writeHead(200, {'Content-Type': 'image/x-icon'})
        res.end( readFileSync(dir + req.url) )
    }

    //// A request for a JSON file.
    else if ('.json' === req.url.slice(-5) && existsSync(dir + req.url) ) {
        res.writeHead(200, {'Content-Type': 'application/json'})
        res.end( readFileSync(dir + req.url) )
    }

    //// A request for a PNG file.
    else if ('.png' === req.url.slice(-4) && existsSync(dir + req.url) ) {
        res.writeHead(200, {'Content-Type': 'image/png'})
        res.end( readFileSync(dir + req.url) )
    }

    //// A request for an SVG file.
    else if ('.svg' === req.url.slice(-4) && existsSync(dir + req.url) ) {
        res.writeHead(200, {'Content-Type': 'image/svg+xml'})
        res.end( readFileSync(dir + req.url) )
    }

    //// A request for a font file.
    else if ('.woff2' === req.url.slice(-6) && existsSync(dir + req.url) ) {
        res.writeHead(200, {'Content-Type': 'font/woff2'})
        res.end( readFileSync(dir + req.url) )
    }

    //// A request for an XML file.
    else if ('.xml' === req.url.slice(-4) && existsSync(dir + req.url) ) {
        res.writeHead(200, {'Content-Type': 'application/xml'})
        res.end( readFileSync(dir + req.url) )
    }

    //// Not found.
    else error(res, 9100, 404, 'No such resource, '+req.url, 'text/plain')
}




//// UTILITY


//// Sends a response after a request which failed.
function error (res, code, status, remarks, contentType=null) {
    const headers = contentType ? { 'Content-Type': contentType } : {}
    remarks = remarks.replace(/\\/g, '\\\\')
    remarks = remarks.replace(/"/g, '\\"')
    remarks = remarks.replace(/\n/g, '\\n')
    res.writeHead(status, headers)
    res.end(`{ "code":${code}, "error":"`
      + `${remarks}", "status":${status} }\n`)
    return false
}
